import React, { Component } from 'react';
import Header from './components/Header';
import SideBar from './components/SideBar';
import step1 from './components/step1';
import step2 from './components/step2';
import step3 from './components/step3';
import step4 from './components/step4';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom'

class App extends Component {

  render() {
    return (
            <Router>
        <div>
            <Header />
        <SideBar />

          <Switch>
            <Route exact path="/" component={step1} />
            <Route path="/step2" component={step2} />
            <Route path="/step3" component={step3} />
            <Route path="/step4" component={step4} />
          </Switch>


        </div>
      </Router>
     
    );
  }
}

export default App;
