import React, {Component} from 'react';
import step2 from './step2';
//import {Input, TextArea, GenericInput} from 'react-text-input'; // ES6
export default class step1 extends Component {
    render(){
        return (
            <div className="content-wrapper">
                <section className="content-header">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="box">
                                <div className="box-header with-border">
                                    <h3 className="box-title">Параметры ипотеки</h3>
                                    <br />
                                    <small>Шаг 1 из 4</small>
                                </div>
                                <div className="box-body">
                                    <div className="row">
                                        <div className="col-md-8">
                                        <div className="col-xs-4 input-only">
                                      

                                        
<input className="form-control input-lg col-xs-3 border-only" id="inputlg" type="text" placeholder="Фамилия" />
</div>
                                        <div className="col-xs-4 input-only">
<input className="form-control input-lg  border-only" id="inputlg" type="text" placeholder="Имя" />
</div>
                                        <div className="col-xs-4 input-only">
<input className="form-control input-lg  border-only" id="inputlg" type="text" placeholder="Отчество" />
</div>

<br /><br />
                                       <div className="col-xs-2">
                                      
                                       <a href="/step2" className="btn btn-block btn-primary btn-lg input-top">Далее </a>
</div>

                                            <p className="text-center">
                                                <strong></strong>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div className="box-footer">
                                    <div className="row">
                                        <div className="col-sm-3 col-xs-6 center-block">
                                            <div className="description-block border-right">
                                               Список банков: 
                                               <div className="container">
    <div className="row">
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/sber.jpg" alt="..." />
            </a>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/psb.gif" alt="..." />
            </a>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/vtb.jpg" alt="..." />
            </a>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/spb.jpg" alt="..." />
            </a>
        </div>
       <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/sber.jpg" alt="..." />
            </a>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/psb.gif" alt="..." />
            </a>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/vtb.jpg" alt="..." />
            </a>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/spb.jpg" alt="..." />
            </a>
        </div><div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/sber.jpg" alt="..." />
            </a>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/psb.gif" alt="..." />
            </a>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/vtb.jpg" alt="..." />
            </a>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/spb.jpg" alt="..." />
            </a>
        </div>
    </div>
</div>
                                             
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}