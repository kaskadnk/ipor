import React, {Component} from 'react';

//import {Input, TextArea, GenericInput} from 'react-text-input'; // ES6
export default class step2 extends Component {
    render(){
        return (
            <div className="content-wrapper">
                <section className="content-header">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="box">
                                <div className="box-header with-border">
                                    <h3 className="box-title">Параметры ипотеки</h3>
                                    <br />
                                    <small>Шаг 2 из 4</small>
                                </div>
                                <div className="box-body">
                                    <div className="row">
                                        <div className="col-md-8">
                                        <div className="col-xs-3 input-only">
                                        <select className="form-control input-lg select2 border-only">
                  <option selected="selected">РФ</option>
                  <option>Беларусь</option>
                  <option>Украина</option>
                  <option>Казахстан</option>
                  <option>Другое</option>
                </select>
              </div>


                                        <div className="col-xs-3 input-only">
<select className="form-control input-lg select2 border-only" data-placeholder="Пол">
                  <option>Мужчина</option>
                  <option>Женщина</option>
                 
                 
                </select>
</div>
                                        <div className="col-xs-3 input-only">
<input className="form-control input-lg  border-only" id="inputlg" type="number" placeholder="Возраст" />
</div>
                                        <div className="col-xs-3 input-only">
<input className="form-control input-lg  border-only" id="inputlg" type="text" placeholder="Стаж" />
</div>
<br /><br />
                                       <div className="col-xs-2">
                                       <a href="/" className="btn btn-block btn-primary btn-lg input-top">Назад </a>
</div>
<div className="col-xs-2">
                                       <a href="/step3" className="btn btn-block btn-primary btn-lg input-top">Далее </a>
</div>

                                            <p className="text-center">
                                                <strong></strong>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div className="box-footer">
                                    <div className="row">
                                        <div className="col-sm-3 col-xs-6 center-block">
                                            <div className="description-block border-right">
                                               Список банков: 
                                               <div className="container">
    <div className="row">
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/sber.jpg" alt="..." />
            </a>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/psb.gif" alt="..." />
            </a>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/vtb.jpg" alt="..." />
            </a>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/spb.jpg" alt="..." />
            </a>
        </div>
       <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/sber.jpg" alt="..." />
            </a>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/psb.gif" alt="..." />
            </a>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/vtb.jpg" alt="..." />
            </a>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/spb.jpg" alt="..." />
            </a>
        </div><div className="col-xs-12 col-sm-6 col-md-4 col-lg-1">
            <a href="#" className="thumbnail">
                <img src="/bank_logo/sber.jpg" alt="..." />
            </a>
        </div>
     
    </div>
</div>
                                             
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}